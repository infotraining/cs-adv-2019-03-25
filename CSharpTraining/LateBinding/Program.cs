﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LateBinding
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly asm = null;

            try
            {
                //asm = Assembly.Load("Models");
                asm = Assembly.LoadFrom(
                    @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-25\CSharpTraining\Models\bin\Debug\Models.dll");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //DisplayAssembly(asm);

            BindTypeUsingDynamic(asm);

            Console.ReadLine();
        }

        static void BindTypeUsingDynamic(Assembly asm)
        {
            try
            {
                Type emp1 = asm.GetType("Models.Employee");
                Type g1 = asm.GetType("Models.Employee+PerformanceGrade");

                dynamic o1 = Activator.CreateInstance(emp1, 1, "Maciek", Enum.Parse(g1, "4"), 5000M);

                dynamic o2 = o1.GetId();

                Console.WriteLine(o2);

                o1.GiveRaise(5.0);

                Console.WriteLine(o1);

                Type eh = asm.GetType("Models.EmployeeHelper");

                MethodInfo m3 = eh.GetMethod("BestCheckForPromotions");

                Type genericListType = typeof(List<>);
                Type employeeListType = genericListType.MakeGenericType(emp1);

                dynamic list = Activator.CreateInstance(employeeListType);
                list.Add(o1);
                
                object[] paramaters2 = new object[] {
                    list,
                    new Predicate<object>(x => (int)o1.Grade > 3 )
                };

                m3.Invoke(null, paramaters2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        static void BindType(Assembly asm)
        {
            try
            {
                Type emp1 = asm.GetType("Models.Employee");
                Type g1 = asm.GetType("Models.Employee+PerformanceGrade");

                object[] parameters = { 1, "Maciek", Enum.Parse(g1, "4"), 5000M };

                object o1 = Activator.CreateInstance(emp1, parameters);

                MethodInfo m1 = emp1.GetMethod("GetId");

                object o2 = m1.Invoke(o1, null);

                Console.WriteLine(o2);

                MethodInfo m2 = emp1.GetMethod("GiveRaise");

                m2.Invoke(o1, new object[] { 5.0 });

                Console.WriteLine(o1);

                Type eh = asm.GetType("Models.EmployeeHelper");

                MethodInfo m3 = eh.GetMethod("BestCheckForPromotions");

                Type genericListType = typeof(List<>);
                Type employeeListType = genericListType.MakeGenericType(emp1);

                IList list = (IList)Activator.CreateInstance(employeeListType);
                list.Add(o1);

                PropertyInfo pi1 = emp1.GetProperty("Grade");

                object[] paramaters2 = new object[] {
                    list,
                    new Predicate<object>(x => (int)pi1.GetValue(x) > 3 )
                };

                m3.Invoke(null, paramaters2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        static void DisplayAssembly(Assembly assembly)
        {
            if (assembly != null)
            {
                Console.WriteLine($"-- Lista typów dla {assembly.FullName} --");

                foreach (Type type in assembly.GetTypes())
                {
                    Console.WriteLine($"  {type.Name}");

                    var attributes = type.GetCustomAttributes();

                    foreach (var attr in attributes)
                    {
                        Console.WriteLine($"    {attr}");
                    }
                }
            }
        }
    }
}
