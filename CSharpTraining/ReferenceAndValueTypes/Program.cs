﻿using Models;
using System;

namespace ReferenceAndValueTypes
{
    class Program
    {
        static void Main(string[] args)
        {
            //ValueTypes();
            //ReferenceTypes();

            //Employee emp = new Employee(2, "Aneta");

            //ChangeName(ref emp);

            //Console.WriteLine(emp.Name);

            DefaultObjectBehaviour();

            Console.ReadLine();
        }

        static void DefaultObjectBehaviour()
        {
            Employee emp1 = new Employee(5, "Mateusz");

            Employee emp2 = (Employee)emp1.Clone();

            Console.WriteLine(emp1.ToString());

            Console.WriteLine(emp1.GetHashCode() == emp2.GetHashCode());

            Console.WriteLine(emp1.GetType());

            Console.WriteLine();

            Console.WriteLine(emp1 == emp2);
            Console.WriteLine(emp1.Equals(emp2));

            Console.WriteLine();

            string o1 = "SzkolenieC#";
            string o2 = new string("SzkolenieC#".ToCharArray());
            
            Console.WriteLine(o1 == o2);
            Console.WriteLine(o1.Equals(o2));
        }

        static void ChangeName(ref Employee employee)
        {
            employee.Name = "Ewa";

            employee = new Employee(3, "Piotr");
        }

        static void ReferenceTypes()
        {
            Employee emp1 = new Employee(1, "Robert");

            Employee emp2 = (Employee)emp1.Clone();

            Console.WriteLine(emp1.Name + ", " + emp1.Contract.GetContractId());
            Console.WriteLine(emp2.Name + ", " + emp2.Contract.GetContractId());

            Console.WriteLine();

            emp2.Name = "Maciek";
            emp2.Contract.Salary = 5000M;

            Console.WriteLine(emp1.Name + ", " + emp1.Contract.GetContractId());
            Console.WriteLine(emp2.Name + ", " + emp2.Contract.GetContractId());
        }//nie ma żadnej gwarancji że 'emp1' zostanie usunięte

        static void ValueTypes()
        {
            int i = 0;

            Point p1 = new Point(10, 10, "Pierwszy punkt");

            Point p2 = p1;

            p1.PrintPosition();
            p2.PrintPosition();

            Console.WriteLine();

            p2.Move(50, 50);
            p2.Info.Description = "Drugi punkt";

            p1.PrintPosition();
            p2.PrintPosition();

        }// usunięta `i` a później `p1`, a później 'p2'
    }
}
