﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionTrees
{
    class DisplayVisitor : ExpressionVisitor
    {
        private int level = 0;

        public override Expression Visit(Expression node)
        {
            if (node != null)
            {
                for (int i = 0; i < level; i++)
                {
                    Console.Write("   ");
                }

                Console.WriteLine($"{node.NodeType} - {node.GetType().ToString()}");
            }

            level++;
            Expression result = base.Visit(node);
            level--;

            return result;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Expression<Func<int, int>> exp = n => 1 + Square(n * ((n % 5) == 0 ? 5 : -5));

            DisplayVisitor visitor = new DisplayVisitor();

            visitor.Visit(exp);

            Console.ReadLine();
        }

        static int Square(int x)
        {
            return x * x;
        }

        static void Basics()
        {
            Func<int, int, int> multiply1 = (a, b) => a * b;

            Console.WriteLine(multiply1(5, 15));
            Console.WriteLine();

            Expression<Func<int, int, int>> multiply2 = (a, b) => a * b;

            Console.WriteLine(multiply2.Compile()(5, 15));

            Console.WriteLine();

            Console.WriteLine(multiply2);
            Console.WriteLine(multiply2.NodeType);
            Console.WriteLine();
            Console.WriteLine(multiply2.Body);
            Console.WriteLine(multiply2.Body.NodeType);
            Console.WriteLine();

            BinaryExpression binaryExpression = (BinaryExpression)multiply2.Body;
            Console.WriteLine(binaryExpression.Left.NodeType);
            Console.WriteLine(binaryExpression.Right.NodeType);
        }
    }
}
