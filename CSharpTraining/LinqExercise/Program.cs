﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LinqExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            //deserializacja binary pliku files.dat do listy typów CustomFileInfo

            BinaryFormatter bf = new BinaryFormatter();

            List<CustomFileInfo> files;

            using (FileStream fs = new FileStream("files.dat", FileMode.Open))
            {
                files = (List<CustomFileInfo>)bf.Deserialize(fs);
            }

            //wszystkie biblioteki dll - ilość

            Console.WriteLine(files.Where(f => f.Extension.ToLower() == ".dll").Count());

            //czy są pliki większe niż 35MB

            Console.WriteLine(files.Any(f => f.Length > 35*1000*1000));

            //najstarszy plik exe - wypisać informacje

            Console.WriteLine(files.Where(f => f.Extension.ToLower() == ".exe")
                .OrderBy(n => n.CreationTime).First().Name);

            //pogrupuj pliki wg rozszerzenia i wypisz od największych do najmniejszych grup

            Console.WriteLine();

            var byExtension = from f in files
                              group f by f.Extension.ToLower()
                              into grouping
                              orderby grouping.Count() descending
                              select new { Extension = grouping.Key, Count = grouping.Count() };

            foreach (var item in byExtension)
            {
                Console.WriteLine($"{item.Extension} - {item.Count}");
            }

            Console.WriteLine();

            //pogrupuj wg pierwszej litery w nazwie pliku i utwórz słownik z 5 najpopularniejszymi literami
            //klucz to litera, wartość to ilość plików zaczynających się od tej litery 
            //.ToDictionary(g => g.Key, g => g.Count())

            var result = (from f in files
                          group f by f.Name.ToLower()[0]
                         into grouping
                          orderby grouping.Count() descending
                          select grouping).Take(5).ToDictionary(g => g.Key, g => g.Count());

            foreach (var item in result)
            {
                Console.WriteLine($"{item.Key} - {item.Value}");
            }

            Console.ReadLine();
        }
    }
}
