﻿using System;

namespace LinqExercise
{
    [Serializable]
    public class CustomFileInfo
    {
        public CustomFileInfo(string name, string extension, long length, DateTime creationTime)
        {
            Name = name;
            Extension = extension;
            Length = length;
            CreationTime = creationTime;
        }

        public string Name { get; set; }
        public string Extension { get; set; }
        //rozmiar - bytes
        public long Length { get; set; }
        public DateTime CreationTime { get; set; }

        public override string ToString()
        {
            return $"{Name}, {Length}, {CreationTime}";
        }
    }
}
