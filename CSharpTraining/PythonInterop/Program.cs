﻿using IronPython.Hosting;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PythonInterop
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables();
            PythonFiles();

            Console.ReadLine();
        }

        static void PythonFiles()
        {
            var engine = Python.CreateEngine();
            var scope = engine.CreateScope();

            var emp1 = new Employee(1, "Weronika");
            Console.WriteLine(emp1.ToString());

            scope.SetVariable("employee", emp1);

            engine.ExecuteFile(
                @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-25\CSharpTraining\PythonInterop\IronPythonApp.py", 
                scope);

            dynamic pythonClass = scope.GetVariable("Person");
            dynamic person = engine.Operations.CreateInstance(pythonClass, 2, "Anna");

            person.show();

            Console.WriteLine(emp1.ToString());
        }

        static void Variables()
        {
            ScriptEngine engine = Python.CreateEngine();
            ScriptScope scope = engine.CreateScope();

            scope.SetVariable("raisePercentage", 5.0);
            scope.SetVariable("employeeSalary", 5000);

            string calculateRaise = "employeeSalary*(1 + raisePercentage/100)";

            ScriptSource source1 = engine.CreateScriptSourceFromString(
                calculateRaise, SourceCodeKind.Expression);

            dynamic newSalary = (decimal)source1.Execute(scope);

            string calculateRaise2 = "employeeNewSalary = employeeSalary*(1 + raisePercentage/100)";

            ScriptSource source2 = engine.CreateScriptSourceFromString(
                calculateRaise2, SourceCodeKind.SingleStatement);

            source2.Execute(scope);

            dynamic newSalary2 = scope.GetVariable("employeeNewSalary");

            Console.WriteLine(newSalary);
            Console.WriteLine(newSalary2);
        }
    }
}
