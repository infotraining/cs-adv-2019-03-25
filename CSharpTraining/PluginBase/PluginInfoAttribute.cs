﻿using System;

namespace PluginBase
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class PluginInfoAttribute : Attribute
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
