﻿namespace PluginBase
{
    public interface ITextTransform
    {
        string Transform(string text);
    }
}
