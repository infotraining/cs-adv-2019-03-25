﻿using Models;
using System.Collections.Generic;

namespace GenericCollections
{
    class SortByNameLentgh : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            //po długości imion
            //mniejsza od zera: x przed y
            //zero: elementy są równe -> później alfabetycznie string.Compare           
            //wieksza od zera: x po y

            if (x.Name.Length > y.Name.Length)
            {
                return -1;
            }
            else if (x.Name.Length < y.Name.Length)
            {
                return 1;
            }
            else
            {
                return string.Compare(x.Name, y.Name);
            }
        }
    }
}
