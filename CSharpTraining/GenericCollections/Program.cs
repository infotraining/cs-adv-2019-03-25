﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GenericCollections
{
    class Program
    {
        static List<Employee> people = new List<Employee>()
        {
            new Employee(1, "Robert"),
            new Employee(2, "Anna"),
            new Employee(3, "Kamil")
        };

        static void Main(string[] args)
        {
            //UseGenericStack();
            //UseSortedSet();
            UseGenericDictionary();

            Console.ReadLine();
        }

        static void UseGenericDictionary()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();

            dict.Add("one", 1);
            dict["two"] = 2;
            dict["two"] = 22;
            dict["three"] = 3;

            Console.WriteLine(dict["two"]);

            Console.WriteLine(dict.ContainsKey("onE"));
            Console.WriteLine(dict.ContainsValue(2));
            Console.WriteLine();

            foreach (KeyValuePair<string,int> item in dict)
            {
                Console.WriteLine($"{item.Key} + {item.Value}");
            }

            Console.WriteLine();

            int i = 0;

            foreach (int item in dict.Values)
            {
                i += item;
            }

            Console.WriteLine(i);
        }

        static void UseSortedSet()
        {
            SortedSet<Employee> employees = new SortedSet<Employee>(new SortByNameLentgh());
            
            employees.Add(people[0]);
            employees.Add(people[1]);
            employees.Add(people[2]);
            employees.Add(people[1]);

            foreach (var item in employees)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void UseGenericStack()
        {
            Stack<Employee> employees = new Stack<Employee>();

            employees.Push(people[0]);
            employees.Push(people[1]);
            employees.Push(people[2]);

            while (employees.Count > 0)
            {
                Console.WriteLine(employees.Peek().Name);
                Console.WriteLine(employees.Pop().ToString());
                Thread.Sleep(1000);
            }
        }

        static void UseGenericList()
        {
            List<Employee> employees = new List<Employee>();
        }
    }
}
