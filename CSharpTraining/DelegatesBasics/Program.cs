﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesBasics
{
    delegate void MessageDelegate(string text);

    class Program
    {
        static void Main(string[] args)
        {
            MessageDelegate del = new MessageDelegate(Message);

            del("Wiadomość z metody Message");

            Console.ReadLine();
        }

        static void Message(string message)
        {
            Console.WriteLine(message);
        }
    }
}
