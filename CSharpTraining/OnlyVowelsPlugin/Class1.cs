﻿using PluginBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlyVowelsPlugin
{
    [PluginInfo(Name = "Tylko samogłoski", Description = "Zostawia same samogłoski")]
    public class OnlyVowelsPlugin : MarshalByRefObject, ITextTransform
    {
        public string Transform(string text)
        {
            string vowels = "aąeęoóuiy";

            return new string(text.ToLower().Where(c => vowels.Contains(c)).ToArray());
        }
    }
}
