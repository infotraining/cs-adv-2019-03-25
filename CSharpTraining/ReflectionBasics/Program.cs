﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReflectionBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            Type type1 = typeof(Employee);

            Console.WriteLine(type1.Assembly);

            Employee emp1 = new Employee(1, "Robert", Employee.PerformanceGrade.Good, 5000M);

            Type type2 = emp1.GetType();

            Console.WriteLine(type2.Name);

            Type type3 = Type.GetType("Models.Contract, Models");

            Console.WriteLine(type3.Name);

            Type type4 = Type.GetType("Models.Employee+PerformanceGrade, Models");

            Console.WriteLine(type4.Name);

            Console.ReadLine();
        }
    }
}
