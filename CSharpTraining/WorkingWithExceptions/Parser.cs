﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WorkingWithExceptions
{
    public class Parser
    {
        public int Parse(string link)
        {
            int i = 0;

            try
            {
                Downloader d = new Downloader();

                XDocument doc = d.Download(link);

                i = doc.Root.DescendantNodes().Count();
            }
            catch (Exception ex)
            {
                //logowanie do dziennika błędów

                throw;
            }

            return i;
        }
    }
}
