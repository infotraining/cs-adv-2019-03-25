﻿using System.Xml.Linq;

namespace WorkingWithExceptions
{
    public class Downloader
    {
        public XDocument Download(string link)
        {
            XDocument xdoc = XDocument.Load(link);

            return xdoc;
        }
    }
}
