﻿using Models;
using System;
using System.IO;

namespace WorkingWithExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            ExceptionsBasics();
            //RethrowingException();           

            Console.ReadLine();
        }

        static void ExceptionsBasics()
        {
            Tank t = new Tank("Leopard");

            t.TurnKey(true);

            try
            {
                for (int i = 0; i < 10; i++)
                {
                    t.SpeedUp(20);
                }
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (TankOverheatedException tox) when (tox.TimeStamp.DayOfWeek == DayOfWeek.Monday)
            {
                Console.WriteLine(tox.Message);
                Console.WriteLine(tox.TimeStamp);
                Console.WriteLine(tox.Cause);

                try
                {
                    FileStream fs = File.Open(@"C:\tank_log.xml", FileMode.Open);
                }
                catch (FileNotFoundException ex)
                {
                    Console.WriteLine(ex.Message);

                    TankOverheatedException tox1 =
                        new TankOverheatedException(tox.Message, ex, tox.TimeStamp, tox.Cause);

                    //zalogowanie tego błędu np. za pomocą serializacji
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //Console.WriteLine(ex.Source);
                //Console.WriteLine(ex.TargetSite);
                //Console.WriteLine(ex.TargetSite.DeclaringType);
                //Console.WriteLine();
                //Console.WriteLine(ex.StackTrace);
                //Console.WriteLine();
                //Console.WriteLine(ex.HelpLink);
                //Console.WriteLine();
                //if (ex.Data != null)
                //{
                //    foreach (DictionaryEntry item in ex.Data)
                //    {
                //        Console.WriteLine($"{item.Key} -> {item.Value}");
                //    }
                //}
            }
            finally
            {
                t.TurnKey(false);
            }

            //t.TurnKey(true);

            //t.SpeedUp(30);
        }

        static void RethrowingException()
        {
            Parser p = new Parser();

            try
            {
                int i = p.Parse(@"http://infotraining.pl/sitemp.xml");

                Console.WriteLine(i);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine();
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
