﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynamicObjectDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic tank = new Tank();

            tank.Aim();
            tank.Shoot();

            Console.WriteLine();

            XElement element = XElement.Parse(
                @"<employee id='1'><name>Łukasz Zawadzki</name></employee>");

            dynamic attributes = element.DynamicAttributes();

            attributes.id = 9;

            Console.WriteLine(attributes.id);

            attributes.state = "active";

            Console.WriteLine(attributes.state);

            Console.ReadLine();
        }
    }

    class Tank : DynamicObject
    {
        public override bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            Console.WriteLine("Została wywołana metoda " + binder.Name);
            result = null;
            return true;
        }
    }
}
