﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamicBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic c = 10;

            c = "Zmienna c";

            Console.WriteLine(c.Substring(0, 3));

            Console.WriteLine(c.GetType());

            Console.ReadLine();
        }
    }

    class TotallyDynamicClass
    {
        private dynamic DynamicField;

        public dynamic DynamicProperty { get; set; }

        public dynamic DynamicMethod(dynamic parameter)
        {
            if (parameter is int)
            {
                return parameter;
            }
            else
            {
                return "Nasz parametr nie jest typu int!";
            }
        }

    }
}
