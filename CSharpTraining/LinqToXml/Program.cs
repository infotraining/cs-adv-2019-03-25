﻿using LinqToEF;
using System;
using System.Linq;
using System.Xml.Linq;

namespace LinqToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            //Parsing();
            //Loading();
            //ManualConstruction();
            //FunctionalConstruction();
            //Navigation();
            //UpdatingXml();
            DatabaseProjection();

            Console.ReadLine();
        }

        static void DatabaseProjection()
        {
            using (StoreDatabaseContext context = new StoreDatabaseContext())
            {
                var query = new XElement("stores",
                    from s in context.Stores.ToList()
                    select new XElement(s.Name, new XAttribute("id", s.ID),
                        from p in s.Products
                        select new XElement(p.Description, new XAttribute("price", p.Price))));

                XDocument doc = new XDocument(query);

                Console.WriteLine(doc);
            }
        }

        static void UpdatingXml()
        {
            XDocument doc = XDocument.Load("employees.xml");

            doc.Root.Element("director").Element("firstName").Value = "Robert";
            
            foreach (var item in doc.Root.Elements("programmer"))
            {
                foreach (var subItem in item.Elements("lastName"))
                {
                    subItem.Value = subItem.Value.ToUpper();
                }
            }

            doc.Root.Element("manager").SetAttributeValue("state", "active");
            doc.Root.Element("manager").SetElementValue("city", "Dąbrowa Górnicza");

            XElement element1 = new XElement("intern",
                new XElement("firstName", "Ewa"),
                new XElement("lastName", "Kowalska"));

            //doc.Root.AddFirst(element1);

            doc.Root.Element("director").ReplaceWith(element1);

            Console.WriteLine(doc);

            doc.Save("employees_updated.xml");
        }

        static void Navigation()
        {
            XDocument doc = XDocument.Load("employees.xml");
        
            foreach (var item in doc.Root.Elements("programmer"))
            {
                Console.WriteLine(item + " ! ");
            }

            Console.WriteLine();

            Console.WriteLine(doc.Root.Element("programmer"));
            Console.WriteLine();

            var query1 = from s in doc.Root.Elements()
                         where s.Attribute("state").Value == "inactive"
                         select s;

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            var query2 = from s in doc.Root.Elements()
                         where s.Elements().Any(c => c.Value == "Maciej")
                         select s;

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            Console.WriteLine(doc.Root.Elements().Descendants("firstName").Count());

            Console.WriteLine(doc.DescendantNodes().OfType<XElement>().Count());
        }

        static void Loading()
        {
            XDocument doc = XDocument.Load("http://infotraining.pl/sitemap.xml");

            foreach (var item in doc.Elements())
            {
                Console.WriteLine(item);
            }
        }

        static void Parsing()
        {
            string xml = @"<employee id='1'><name>Jakub</name></employee>";

            XDocument doc = XDocument.Parse(xml);

            Console.WriteLine(doc);

            Console.WriteLine();

            Console.WriteLine(doc.Root.Element("name"));
        }

        static void ManualConstruction()
        {
            //ręczne stworzenie xml
            XElement employee1 = new XElement("employee");

            XAttribute attr1 = new XAttribute("id", 1);
            XAttribute attr2 = new XAttribute("state", "active");

            XComment comm = new XComment("idealny pracownik");

            XElement firstName = new XElement("firstName", "Jakub");
            XElement lastName = new XElement("lastName", "Nowak");

            employee1.Add(comm);
            employee1.Add(attr1);
            employee1.Add(attr2);
            employee1.Add(firstName);
            employee1.Add(lastName);

            XElement employee2 = new XElement("employee");

            employee2.Add(firstName);

            employee2.Element("firstName").Value = "Robert";

            XDocument doc1 = new XDocument(employee1);

            foreach (XElement item in doc1.Elements())
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            //automatyczne głębokie kopiowanie
        }

        static void FunctionalConstruction()
        {
            //stworzenie za pomocą funkcji
            XElement employees = new XElement("employees", new XComment("lista pracowników"),
                new XElement("director",
                    new XAttribute("id", 1), new XAttribute("state", "active"), new XComment("dyrektor działu programistów"),
                    new XElement("firstName", "Jakub"),
                    new XElement("lastName", "Nowak")),
                new XElement("manager",
                    new XAttribute("id", 2), new XAttribute("state", "inactive"), new XComment("manager projektu"),
                    new XElement("firstName", "Marcin"),
                    new XElement("lastName", "Kowalski")),
                new XElement("programmer",
                    new XAttribute("id", 3), new XAttribute("state", "active"),
                    new XElement("firstName", "Robert"),
                    new XElement("lastName", "Makowski")),
                new XElement("programmer",
                    new XAttribute("id", 4), new XAttribute("state", "active"),
                    new XElement("firstName", "Maciej"),
                    new XElement("lastName", "Grzanka"))
            );

            XDocument xDoc = new XDocument(employees);

            xDoc.Save("employees.xml");

            Console.WriteLine(xDoc);
        }
    }
}
