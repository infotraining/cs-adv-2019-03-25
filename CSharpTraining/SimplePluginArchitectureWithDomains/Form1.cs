﻿using PluginBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplePluginArchitecture
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.openFileDialog1.Filter = ".dll files|*.dll";
            this.textBox1.Text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";
            this.LoadedDomains = new List<AppDomain>();
        }

        private int y = 20;
        public List<AppDomain> LoadedDomains { get; set; }

        private PluginInfoAttribute GetPluginInfo(Type t)
        {
            //implementacja metody zwracającej atrybut - metoda GetCustomAttributes

            object info = t.GetCustomAttributes(false)
                .Where(x => x.GetType() == typeof(PluginInfoAttribute))
                .First();

            return (PluginInfoAttribute)info;
        }

        private void click(object sender, EventArgs e, ITextTransform plugin)
        {
            //implementacja metody kliknięcia przycisku - właściwość textBox1.SelectedText
            try
            {
                textBox1.SelectedText = plugin.Transform(textBox1.SelectedText);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private bool LoadPlugin(string path)
        {
            bool isSuccess = false;
            
            Assembly asm = null;

            try
            {
                //załadowanie zestawu na podstawie ścieżki - metoda LoadFrom

                //wybranie typów - GetTypes - implementujących interfejs ITextTransform do kolekcji 'IEnumerable<Type> plugins'

                asm = Assembly.LoadFrom(path);

                var plugins = asm.GetTypes().Where(t => t.IsClass && t.GetInterface("ITextTransform") != null);

                foreach (Type t in plugins)
                {
                    //stworzenie instancji typu i przypisanie do zmiennej 'ITextTransorm plugin'

                    if (LoadedDomains.Select(ld => ld.FriendlyName).Contains(t.FullName))
                        continue;

                    AppDomainSetup setup = new AppDomainSetup
                    {
                        ApplicationBase = Path.GetDirectoryName(t.Module.FullyQualifiedName)
                    };

                    PermissionSet permissionSet = new PermissionSet(PermissionState.None);

                    permissionSet.AddPermission(
                        new FileIOPermission(FileIOPermissionAccess.Read | FileIOPermissionAccess.PathDiscovery, Path.GetDirectoryName(t.Module.FullyQualifiedName)));

                    permissionSet.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));

                    AppDomain ad = AppDomain.CreateDomain(t.FullName, null, setup, permissionSet);

                    LoadedDomains.Add(ad);

                    ITextTransform plugin = (ITextTransform)
                        ad.CreateInstanceFromAndUnwrap(t.Module.FullyQualifiedName, t.FullName);

                    Button button = new Button
                    {
                        Text = GetPluginInfo(t).Name,
                        Location = new Point(5, y),
                        Tag = ad.FriendlyName,
                        Width = 150
                    };

                    y += 25;

                    button.Click += new EventHandler((s, e) => click(s, e, plugin));

                    ToolTip tooltip = new ToolTip
                    {
                        Tag = ad.FriendlyName
                    };

                    tooltip.SetToolTip(button, GetPluginInfo(t).Description);

                    groupBox1.Controls.Add(button);

                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isSuccess;
        }

        private void loadPluginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (!LoadPlugin(openFileDialog1.FileName))
                {
                    MessageBox.Show("Plik nie jest kompatybilny albo został już załadowany!");
                }
            }
        }

        private void removePluginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (LoadedDomains.Any())
            {
                Form2 f = new Form2(this);
                f.Show();
            }
            else
            {
                MessageBox.Show("Nie załadowano żadnych pluginów");
            }
        }
    }
}
