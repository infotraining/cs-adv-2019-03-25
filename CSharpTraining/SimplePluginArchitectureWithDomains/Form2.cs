﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SimplePluginArchitecture
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private Form1 f1;

        public Form2(Form1 form1)
        {
            InitializeComponent();

            if (form1 != null)
            {
                f1 = form1;

                foreach (AppDomain item in f1.LoadedDomains)
                {
                    checkedListBox1.Items.Add(item);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (object item in this.checkedListBox1.CheckedItems)
            {
                AppDomain domain = (AppDomain)item;

                Control groupBox = f1.Controls.Find("groupBox1", true).First();

                IEnumerable<Control> buttons = groupBox.Controls.Cast<Control>()
                    .Where(x => Convert.ToString(x.Tag) == domain.FriendlyName);

                foreach (var control in buttons)
                {
                    f1.Controls.Remove(control);
                    control.Dispose();
                }

                f1.LoadedDomains.Remove(domain);

                AppDomain.Unload(domain);
            }

            this.Close();
        }
    }
}
