﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ExpandoObjectDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic expando = new ExpandoObject();

            expando.Name = "Marcin";
            expando.Id = 1;
            expando.Salary = 5000M;
            expando.GiveRaise = new Action(() => expando.Salary = expando.Salary + 500M);

            expando.GiveRaise();

            foreach (var item in expando)
            {
                Console.WriteLine($"{item.Key} - {item.Value}");
            }

            Console.WriteLine();

            XDocument xdoc = XDocument.Load(@"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-25\CSharpTraining\LinqToXml\bin\Debug\employees.xml");

            foreach (var item in xdoc.Root.Elements())
            {
                Console.WriteLine(item.Element("firstName").Value);
            }

            Console.WriteLine();

            dynamic xdocExpando = xdoc.AsExpando();

            foreach (var item in xdocExpando.employees)
            {
                Console.WriteLine(item.firstName);
            }

            Console.ReadKey();
        }
    }
}
