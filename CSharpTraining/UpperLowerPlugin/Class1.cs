﻿using PluginBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UpperLowerPlugin
{
    [PluginInfo(Name = "Małe litery", Description = "Zamienia tekst na małe litery")]
    public class ToLowerPlugin : MarshalByRefObject, ITextTransform 
    {
        public string Transform(string text)
        {
            string result = File.ReadAllText(
                @"C:\Users\lkz\Documents\Repos\cs-adv-2019-03-25\CSharpTraining\LinqToXml\bin\Debug\employees.xml");

            return result;
        }
    }

    [PluginInfo(Name = "Duże litery", Description = "Zamienia tekst na duże litery")]
    public class ToUpperPlugin : MarshalByRefObject, ITextTransform
    {
        public string Transform(string text)
        {
            return text.ToUpper();
        }
    }
}
