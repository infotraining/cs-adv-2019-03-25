﻿using System;
using System.Reflection;

namespace TypeExplorer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---- Explorator metadanych typu ----");
            string name = "";

            while (true)
            {
                Console.WriteLine("Podaj nazwę typu: ");
                name = Console.ReadLine();

                if (name == "q")
                {
                    break;
                }

                try
                {
                    Type t = Type.GetType(name);

                    Console.WriteLine();
                    Console.WriteLine($"{t.Name}");
                    Console.WriteLine();
                    Console.WriteLine($"{t.Assembly.FullName}");
                    Console.WriteLine();

                    ShowGeneralInfo(t);
                    ShowFields(t);
                    ShowProperties(t);
                    ShowMethods(t);
                    ShowInterfaces(t);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        static void ShowGeneralInfo(Type t)
        {
            Console.WriteLine("-- Podstawowe informacje --");
            Console.WriteLine($"Czy jest klasą? {t.IsClass}");
            Console.WriteLine($"Czy jest interfejsem? {t.IsInterface}");
            Console.WriteLine($"Czy typ jest abstrakcyjny? {t.IsAbstract}");
        }

        public static void ShowMethods(Type t)
        {
            Console.WriteLine("-- Metody dla typu --");

            MethodInfo[] methods = t.GetMethods(/*BindingFlags.NonPublic | BindingFlags.Static*/);

            foreach (MethodInfo method in methods)
            {
                string val = method.ReturnType.FullName;

                string parameters = "(";

                foreach (ParameterInfo parameterInfo in method.GetParameters())
                {
                    parameters += $"{parameterInfo.ParameterType} {parameterInfo.Name}";
                }

                parameters += ")";

                Console.WriteLine($"  {val} {method.Name} {parameters}");
            }

            Console.WriteLine();
        }

        static void ShowFields(Type t)
        {
            Console.WriteLine("-- Pola --");

            FieldInfo[] fields = t.GetFields();

            foreach (FieldInfo field in fields)
            {
                Console.WriteLine($"  {field.Name}");
            }

            Console.WriteLine();
        }

        static void ShowProperties(Type t)
        {
            Console.WriteLine("-- Właściwości --");

            PropertyInfo[] properties = t.GetProperties();

            foreach (PropertyInfo property in properties)
            {
                Console.WriteLine($"  {property.Name}");
            }

            Console.WriteLine();
        }

        static void ShowInterfaces(Type t)
        {
            Console.WriteLine("-- Interfejsy --");

            var interfaces = t.GetInterfaces();

            foreach (var item in interfaces)
            {
                Console.WriteLine($"  {item}");
            }

            Console.WriteLine();
        }
    }
}
