﻿using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Serialization
{
    class Program
    {
        static void Main(string[] args)
        {
            Employee emp1 = new Employee(4, "Bartosz", Employee.PerformanceGrade.Good, 5000M);

            Console.WriteLine("Przed serializacją:");
            Console.WriteLine(emp1.ToString());
            Console.WriteLine(emp1.Contract.GetContractId());
            Console.WriteLine();

            BinaryFormatter bf = new BinaryFormatter();
            string filename1 = "employee.dat";

            Serialize(emp1, filename1, bf);

            Employee emp2 = Deserialize(filename1, new BinaryFormatter());

            Console.WriteLine("Po deserializacji *binary*");
            Console.WriteLine(emp2.ToString());
            Console.WriteLine(emp2.Contract.GetContractId());
            Console.WriteLine();

            SoapFormatter sf = new SoapFormatter();
            string filename2 = "employee.soap";

            Serialize(emp1, filename2, sf);

            Employee emp3 = Deserialize(filename2, sf);
            
            Console.WriteLine("Po deserializacji *soap*");
            Console.WriteLine(emp3.ToString());
            Console.WriteLine(emp3.Contract.GetContractId());
            Console.WriteLine();

            string filename3 = "employee.xml";

            Serialize(emp1, filename3, null);

            Employee emp4 = Deserialize(filename3, null);

            Console.WriteLine("Po deserializacji *xml*");
            Console.WriteLine(emp4.ToString());
            Console.WriteLine(emp4.Contract.GetContractId());
            Console.WriteLine();

            Employee emp5 = JsonSerialization(emp1);

            Console.WriteLine("Po deserializacji *json*");
            Console.WriteLine(emp5.ToString());
            Console.WriteLine(emp5.Contract.GetContractId());
            Console.WriteLine();

            SerializeCollection();

            Console.ReadLine();
        }

        static void SerializeCollection()
        {
            List<Employee> employees = new List<Employee>();

            employees.Add(new Employee(1, "Marcin", Employee.PerformanceGrade.Good, 5000M));
            employees.Add(new Employee(2, "Maciek", Employee.PerformanceGrade.Decent, 4000M));
            employees.Add(new Employee(3, "Mateusz", Employee.PerformanceGrade.Stellar, 6000M));

            BinaryFormatter bf = new BinaryFormatter();

            using (FileStream fs = new FileStream("employees.dat", FileMode.Create))
            {
                bf.Serialize(fs, employees);
            }

            List<Employee> list = new List<Employee>();

            using (FileStream fs = new FileStream("employees.dat", FileMode.Open))
            {
                list = (List<Employee>)bf.Deserialize(fs);
            }

            foreach (Employee item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }

        static Employee JsonSerialization(Employee emp)
        {
            JsonSerializer js = new JsonSerializer();

            using (StreamWriter sw = new StreamWriter("employee.json"))
            using (JsonWriter jw = new JsonTextWriter(sw))
            {
                js.Serialize(jw, emp);
            }

            Employee empOut;

            using (StreamReader sr = new StreamReader("employee.json"))
            using (JsonReader jr = new JsonTextReader(sr))
            {
                empOut = js.Deserialize<Employee>(jr);
            }

            return empOut;
        }

        static void Serialize(Employee emp, string filename, IFormatter formatter)
        {
            if (formatter != null)
            {
                using (FileStream fs = new FileStream(filename, FileMode.Create))
                {
                    formatter.Serialize(fs, emp);
                }
            }
            else
            {
                XmlSerializer xs = new XmlSerializer(typeof(Employee));

                using (FileStream fs = new FileStream(filename, FileMode.Create))
                {
                    xs.Serialize(fs, emp);
                }
            }
        }

        static Employee Deserialize(string filename, IFormatter formatter)
        {
            Employee emp;

            if (formatter != null)
            {
                using (FileStream fs = new FileStream(filename, FileMode.Open))
                {
                    emp = (Employee)formatter.Deserialize(fs);

                    return emp;
                }
            }
            else
            {
                XmlSerializer xs = new XmlSerializer(typeof(Employee));

                using (FileStream fs = new FileStream(filename, FileMode.Open))
                {
                    emp = (Employee)xs.Deserialize(fs);

                    return emp;
                }
            }
        }
    }
}
