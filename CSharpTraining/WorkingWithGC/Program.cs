﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WorkingWithGC
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateEmployee();

            EmployeeForGC emp1 = new EmployeeForGC(5, "Maria", Employee.PerformanceGrade.Decent);

            Console.WriteLine(Marshal.PtrToStringAnsi(emp1.EmployeeInfo));
            Console.WriteLine(emp1.Connection.ConnectionString);

            emp1.Dispose();

            Console.WriteLine();
            Console.WriteLine(Marshal.PtrToStringAnsi(emp1.EmployeeInfo));
            Console.WriteLine(emp1.Connection.ConnectionString);

            //using (EmployeeForGC emp3 = new EmployeeForGC(11, "Marian", Employee.PerformanceGrade.Good))
            //{
            //    Console.WriteLine(emp3.Connection.ConnectionString);
            //}

            EmployeeForGC emp3 = new EmployeeForGC(11, "Marian", Employee.PerformanceGrade.Good);

            try
            {
                Console.WriteLine(emp3.Connection.ConnectionString);
            }
            finally
            {
                emp3.Dispose();
            }

            //GC.Collect();
            //GC.WaitForPendingFinalizers();

            Console.ReadLine();
        }

        static void CreateEmployee()
        {
            EmployeeForGC emp2 = new EmployeeForGC(3, "Konrad", Employee.PerformanceGrade.Stellar);

            Console.WriteLine(Marshal.PtrToStringAnsi(emp2.EmployeeInfo));
        }//nie mamy żadnej gwarancji że 'emp2' zostanie usunięte
    }
}
