﻿using LinqToEF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LinqOperators
{
    class Program
    {
        static string[] names = { "Kamil Stoch", "Piotr Żyła", "Dawid Kubacki", "Maciek Kot" };
        static int[] numbers = { 2, 15, 67, 4, 6, 15, 4 };

        static readonly StoreDatabaseContext context = new StoreDatabaseContext();

        static void Main(string[] args)
        {
            //Filtering();
            //Projection();
            //Joining();
            //Sorting();
            //Grouping();
            ElementOperators();

            Console.ReadLine();
        }

        static void ElementOperators()
        {
            Console.WriteLine(numbers.FirstOrDefault(n => n > 100));
            Console.WriteLine();
            Console.WriteLine(numbers.SingleOrDefault(n => n % 10 == 0));
            Console.WriteLine();

            //operatory agregacji

            Console.WriteLine(numbers.Count(n => n > 10));
            Console.WriteLine(numbers.Max(n => n < 50));
            Console.WriteLine(numbers.Sum());
            Console.WriteLine(numbers.Average());
            Console.WriteLine();

            //kwantyfikatory
            Console.WriteLine(numbers.Any(n => n < 0));
            Console.WriteLine(numbers.All(n => n > 0));
            Console.WriteLine(numbers.Contains(3));
        }

        static void Grouping()
        {
            var query1 = from p in context.Products
                         group p by new { p.Description, p.Price } into grouping
                         select new
                         {
                             Description = grouping.Key,
                             Stores = grouping.Select(p => p.Store.Name)
                         };

            foreach (var item in query1)
            {
                Console.WriteLine(item.Description);

                foreach (var subItem in item.Stores)
                {
                    Console.WriteLine($"-> {subItem}");
                }
            }
        }

        static void Sorting()
        {
            var query = from n in names
                        orderby n[1], n.Length
                        select n;

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }
        }

        static void Joining()
        {
            //join
            var query1 = from s in context.Stores
                         join p in context.Products on s.ID equals p.StoreID
                         where p.Price > 1500M
                         select "Telefon " + p.Description + " można kupić w sklepie " + s.Name;

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            //groupjoin
            var query2 = from s in context.Stores
                         join p in context.Products.Where(p => p.Price > 1500M) on s.ID equals p.StoreID
                         into products
                         where products.Any()
                         select new { s.Name, products };

            foreach (var item in query2)
            {
                Console.WriteLine(item.Name);

                foreach (var subItem in item.products)
                {
                    Console.WriteLine("-> " + subItem.Description);
                }
            }

        }

        static void Projection()
        {
            //var query1 = from s in context.Stores
            //             where s.Products.Any(p => p.Price > 1500M)
            //             select new
            //             {
            //                 s.Name,
            //                 Items = from p in s.Products
            //                         where p.Price > 1500M
            //                         select p
            //             };

            var query1 = from s in context.Stores
                         let expensiveProducts = from p in s.Products where p.Price > 1500M select p
                         where expensiveProducts.Any()
                         select new { s.Name, Items = expensiveProducts };                            

            foreach (var item in query1)
            {
                Console.WriteLine(item.Name);

                foreach (var subItem in item.Items)
                {
                    Console.WriteLine($"-> {subItem.Description} - {subItem.Price}");
                }
            }

            Console.WriteLine();

            IEnumerable<string[]> query2 = names.Select(n => n.Split());

            foreach (var item in query2)
            {
                foreach (var subItem in item)
                {
                    Console.WriteLine(subItem);
                }
            }

            Console.WriteLine();

            //IEnumerable<string> query3 = names.SelectMany(n => n.Split());

            var query3 = from n in names
                         from n2 in n.Split()
                         select n2 + " pochodzi z: " + n;

            foreach (var item in query3)
            {
                Console.WriteLine(item);
            }
        }

        static void Filtering()
        {
            var query1 = names.Where((n, i) => i % 2 == 0);

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            foreach (var item in names.Skip(3))
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            foreach (var item in numbers.Distinct())
            {
                Console.Write(item + " ");
            }
        }
    }
}
