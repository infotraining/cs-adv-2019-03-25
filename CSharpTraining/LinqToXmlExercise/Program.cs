﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace LinqToXmlExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument doc1 = XDocument.Load(@"http://rss.nbp.pl/kursy/TabelaA.xml");

            var tables = doc1.Descendants("item").Select(item => 
            new {
                Title = item.Element("title").Value,
                Url = item.Element("enclosure").Attribute("url").Value
            });

            //foreach (var item in tables)
            //{
            //    Console.WriteLine(item.Title + " - url: " + item.Url);
            //}

            string[] currencies = { "USD", "GBP", "EUR", "CAD" };

            // wyswietl dane z koszyka w formacie: 
            // Kod waluty: {0,-5} Nazwa waluty: {1,-20}; Kurs średni: {3:f4}

            XDocument xdoc2 = XDocument.Load(tables.First().Url);

            //zapytanie na xdoc2
            //elementy w 'pozycja': nazwa_waluty, kod_waluty, kurs_sredni

            //var rates = xdoc2.Elements("pozycja").Where()
            var rates = from item in xdoc2.Root.Elements("pozycja")
                        where currencies.Contains(item.Element("kod_waluty").Value)
                        select new
                        {
                            Kod = item.Element("kod_waluty").Value,
                            Nazwa = item.Element("nazwa_waluty").Value,
                            KursSredni = item.Element("kurs_sredni").Value
                        };

            //wypisanie za pomocą foreach

            foreach (var rate in rates)
            {
                Console.WriteLine(
                    $"Kod waluty: {rate.Kod,-5} Nazwa waluty: {rate.Nazwa,-20} " +
                    $"Kurs średni: {rate.KursSredni:f4}");
            }

            Console.ReadLine();
        }
    }
}
