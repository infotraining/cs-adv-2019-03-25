﻿using Models;
using System;
using System.Collections;

namespace CollectionBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            //UseNonGenericArrayList();

            UseEmployeeCollection();

            Console.ReadLine();
        }

        static void Boxing()
        {
            int a = 4;

            object b = a;

            int c = (int)b;
        }

        static void UseEmployeeCollection()
        {
            EmployeeCollection ec = new EmployeeCollection();

            ec.AddEmployee(new Employee(1, "Robert"));
            ec.AddEmployee(new Employee(2, "Maciek"));
            ec.AddEmployee(new Employee(3, "Ewa"));

            //IEnumerator i = ec.GetEnumerator();

            //i.MoveNext();
            //i.MoveNext();
            //i.Reset();

            //Employee emp = (Employee)i.Current;

            //Console.WriteLine(emp.ToString());

            foreach (var item in ec.GetEmployees(true))
            {
                Console.WriteLine(item.ToString());
            }
        }

        static void UseNonGenericArrayList()
        {
            ArrayList arrayList = new ArrayList();

            arrayList.AddRange(new Employee[]
            {
                new Employee(1, "Robert"),
                new Employee(2, "Ewa"),
                new Employee(3, "Marcin")
            });

            arrayList.Add(4);

            Console.WriteLine(arrayList.Count);

            foreach (Employee item in arrayList)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}
