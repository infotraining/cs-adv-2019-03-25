﻿using Models;
using System.Collections;

namespace CollectionBasics
{
    class EmployeeCollection : IEnumerable
    {
        private ArrayList employees = new ArrayList();

        public void AddEmployee(Employee emp)
        {
            employees.Add(emp);
        }

        public Employee GetEmployee(int position)
        {
            return (Employee)employees[position];
        }

        public int Count
        {
            get { return employees.Count; }
        }

        public IEnumerable GetEmployees(bool reversed)
        {
            if (reversed)
            {
                for (int i = employees.Count; i != 0; i--)
                {
                    yield return employees[i - 1];
                }
            }
            else
            {
                foreach (var item in employees)
                {
                    yield return item;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            //return employees.GetEnumerator();

            foreach (Employee item in employees)
            {
                yield return item.Name;
            }

            //yield return employees[0];
            //yield return employees[1];
            //yield return employees[2];
        }

        //public IEnumerator GetEnumerator()
        //{
        //    return employees.GetEnumerator();
        //}
    }
}
