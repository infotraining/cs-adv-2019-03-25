﻿using System;

namespace LinqBasics
{
    public class Movie
    {
        public Movie(string title, double rating, int year)
        {
            Title = title;
            Rating = rating;
            Year = year;
        }

        public string Title { get; set; }
        public int Year { get; set; }

        private double _rating;
        public double Rating
        {
            get
            {
                Console.WriteLine($"Dostęp do oceny filmu -> {Title}");
                return _rating;
            }
            set
            {
                if (value > 0.0)
                    _rating = value;
                else
                    _rating = 0.0;
            }
        }
    }
}
