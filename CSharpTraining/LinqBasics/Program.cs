﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBasics
{
    class Program
    {
        static string[] names = { "Kamil Stoch", "Piotr Żyła", "Dawid Kubacki", "Maciek Kot" };
        static int[] numbers = { 2, 15, 67, 4, 6, 15, 4 };

        static void Main(string[] args)
        {
            //Basics();
            //ChainingQueries();
            //SequenceOrder();
            //SingleElement();
            //DeferredExecution();
            //CustomLinqQuery();
            //CapturedVariables();
            //Subqueries();
            //Composition();
            Projection();

            Console.ReadLine();
        }

        static void Projection()
        {
            IEnumerable<NameProjectionClass> query1 = names.Select(n => new NameProjectionClass {
                Original = n,
                Shortened = ShortenName(n)
            });

            foreach (var item in query1)
            {
                Console.WriteLine($"{item.Shortened} pochodzi z {item.Original}");
            }

            Console.WriteLine();

            var query2 = names.Select(n => new
            {
                Original = n,
                Short = ShortenName(n)
            });

            foreach (var item in query2)
            {
                Console.WriteLine($"{item.Original} przekształca w {item.Short}");
            }

            Console.WriteLine();

            var query3 = from n in names
                         select new
                         {
                             Origin = n,
                             Short = ShortenName(n)
                         }
                         into n2
                         where n2.Short.Length > 8
                         orderby n2.Origin
                         select n2.Origin;

            foreach (var item in query3)
            {
                Console.WriteLine(item);
            }

            var query4 = from n in names
                         let firstName = n.Split().First()
                         let shortened = ShortenName(n)
                         where shortened.Length > 8
                         orderby n
                         select firstName;
            
            foreach (var item in query4)
            {
                Console.WriteLine(item);
            }
        }

        static void Composition()
        {
            ProgressiveQuery("", false, false);

            Console.WriteLine();

            var query1 = names.Select(n => ShortenName(n)).Where(n => n.Length > 8).OrderBy(n => n);

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query2 = from n in names
                         select ShortenName(n);

            query2 = from n in query2
                     where n.Length > 8
                     orderby n
                     select n;

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query3 = from n in names
                         select ShortenName(n)
                         into n2
                         where n2.Length > 8
                         orderby n2
                         select n2;

            foreach (var item in query3)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query4 = from n in (from n2 in names select ShortenName(n2))
                         where n.Length > 8
                         orderby n
                         select n;

            foreach (var item in query4)
            {
                Console.WriteLine(item);
            }
        }

        static string ShortenName(string fullName)
        {
            string[] splitName = fullName.Split();

            return $"{splitName.First().Substring(0, 1)}. {splitName.Last()}";
        }

        static void ProgressiveQuery(string contains, bool order, bool toUpper)
        {
            IEnumerable<string> query = names;

            //zaimplementować
            if (contains != string.Empty)
                query = query.Where(n => n.Contains(contains));

            if (order)
                query = query.OrderBy(n => n.Split().Last());

            if (toUpper)
                query = query.Select(n => n.ToUpper());

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }
        }

        static void Subqueries()
        {
            var query1 = names.OrderBy(n => n.Split().Last());

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }
        }

        static void CapturedVariables()
        {
            int a = 5;

            var query1 = numbers.Select(n => n * a);

            a = 10;

            foreach (var item in query1)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            //dla ilustracji problemu - delegaty
            Action[] actions = new Action[3];

            for (int i = 0; i < actions.Length; i++)
            {
                int j = i;
                actions[i] = () => Console.Write(j + " ");
            }

            //int i = 0;
            //actions[0] = () => Console.Write(i + " ");
            //i = 1;
            //actions[1] = () => Console.Write(i + " ");
            //i = 2;
            //actions[2] = () => Console.Write(i + " ");
            //i = 3;

            foreach (Action action in actions)
            {
                action();
            }
            Console.WriteLine();
            //pokazanie problemu w Linq

            IEnumerable<int> numbers1 = numbers.ToList();

            int[] toExclude = { 6, 15 };

            //for (int i = 0; i < toExclude.Length; i++)
            //{
            //    int j = i;
            //    numbers1 = numbers1.Where(n => n != toExclude[j]);
            //}

            foreach (var item in toExclude)
            {
                numbers1 = numbers1.Where(n => n != item);
            }

            foreach (var item in numbers1)
            {
                Console.Write(item + " ");
            }
        }

        static void DeferredExecution()
        {
            List<int> numbers1 = numbers.ToList();

            List<int> query1 = numbers1.Select(n => n + 10).ToList();

            numbers1.Add(10);

            foreach (var item in query1)
            {
                Console.Write(item + " ");
            }

            Console.WriteLine();

            numbers1.Clear();

            foreach (var item in query1)
            {
                Console.Write(item + " ");
            }
        }

        static void SingleElement()
        {
            Console.WriteLine(numbers.Average());
        }

        static void SequenceOrder()
        {
            foreach (var item in numbers)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();

            foreach (var item in numbers.Where(n => n > 10))
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();

            foreach (var item in numbers.Reverse())
            {
                Console.Write(item + " ");
            }
        }

        static void ChainingQueries()
        {
            //var query1 = names.Where(n => n.Contains("o")).OrderBy(n => n.Length).Select(n => n.ToUpper());

            var filtered = names.Where(n => n.Contains("o"));
            var ordered = filtered.OrderBy(n => n.Length);
            var query1 = ordered.Select(n => n.ToUpper());

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }

            var query2 = from n in names
                         where n.Contains("o")
                         orderby n.Length
                         select n.ToUpper();

            Console.WriteLine();

            foreach (var item in query2)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            var query3 = (from n in numbers
                         where n > 10
                         select n).Average();
        }

        static void Basics()
        {
            IEnumerable<string> query = names.Where(n => n.Length > 10);

            foreach (var item in query)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            IEnumerable<string> query1 = from n in names
                                         where n.Length > 10
                                         select n;

            foreach (var item in query1)
            {
                Console.WriteLine(item);
            }
        }

        static void CustomLinqQuery()
        {
            var movies = new List<Movie>
            {
                new Movie("Green Book", 8.3, 2018),
                new Movie("Shape of Water", 7.3, 2017),
                new Movie("Moonlight", 7.4, 2016),
                new Movie("Spotlight", 8.1, 2015),
                new Movie("Birdman", 7.7, 2014)
            };

            var query = movies.Filter(m => m.Rating > 8.0);

            foreach (var item in query)
            {
                Console.WriteLine(item.Title);
            }
        }
    }
}
