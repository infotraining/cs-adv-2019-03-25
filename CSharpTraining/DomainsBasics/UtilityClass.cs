﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainsBasics
{
    public class UtilityClass : MarshalByRefObject
    {
        public void PrintMessage()
        {
            Console.WriteLine($"Wywołane z: " + AppDomain.CurrentDomain.FriendlyName);
        }
    }
}
