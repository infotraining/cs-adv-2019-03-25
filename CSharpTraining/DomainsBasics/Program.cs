﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainsBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            DomainInfo(AppDomain.CurrentDomain);

            Console.WriteLine();

            CreateDomain();

            Console.WriteLine();

            AppDomain ad = AppDomain.CreateDomain("TrzeciaDomena");

            Type t = typeof(UtilityClass);

            UtilityClass uc = 
                (UtilityClass)ad.CreateInstanceAndUnwrap(t.Assembly.FullName, t.FullName);

            uc.PrintMessage();

            Console.ReadLine();
        }

        static void CreateDomain()
        {
            AppDomain domain = AppDomain.CreateDomain("DrugaDomena");

            try
            {
                domain.Load("DelegatesAdvanced");
                domain.ExecuteAssemblyByName("DelegatesAdvanced");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            DomainInfo(domain);

            AppDomain.Unload(domain);
        }

        static void DomainInfo(AppDomain domain)
        {
            Console.WriteLine(domain.FriendlyName);
            Console.WriteLine(domain.Id);
            Console.WriteLine(domain.BaseDirectory);

            foreach (var assembly in domain.GetAssemblies())
            {
                Console.WriteLine(
                    $"  {assembly.GetName().Name}, wersja: {assembly.GetName().Version}");
            }
        }
    }
}
