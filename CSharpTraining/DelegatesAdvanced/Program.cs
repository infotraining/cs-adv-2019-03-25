﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAdvanced
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>
            {
                new Employee(1, "Bartosz", Employee.PerformanceGrade.Decent, 6000M),
                new Employee(2, "Anna", Employee.PerformanceGrade.Good, 5000M),
                new Employee(3, "Maciek", Employee.PerformanceGrade.Bad, 4500M),
                new Employee(4, "Grzesiek", Employee.PerformanceGrade.Stellar, 7000M)
            };

            EmployeeHelper.CheckForPromotion(employees);

            Console.WriteLine();

            IsPromotable prom = new IsPromotable(PromoteByGrade);

            EmployeeHelper.BetterCheckForPromotions(employees, prom);

            Console.WriteLine();

            EmployeeHelper.BestCheckForPromotions(employees, emp => emp.Grade > Employee.PerformanceGrade.Good);

            Console.WriteLine();

            employees.ForEach(emp => emp.Contract.Salary += 500M);

            foreach (var item in employees)
            {
                Console.WriteLine(item.ToString());
            }

            Func<int, int, string> add = (a, b) => $"Suma to: {a + b}";

            Console.WriteLine();

            Console.WriteLine(add(10, 20));

            Console.WriteLine();

            Console.WriteLine(employees.GetAverageGrade());

            Console.ReadLine();
        }

        static bool PromoteByGrade(Employee employee)
        {
            return employee.Grade > Employee.PerformanceGrade.Good;
        }
    }
}
