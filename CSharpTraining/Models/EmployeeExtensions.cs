﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public static class EmployeeExtensions
    {
        public static double GetAverageGrade(this List<Employee> employees)
        {
            if (employees.Count > 0)
            {
                int sum = 0;

                foreach (var item in employees)
                {
                    sum += (int)item.Grade;
                }

                return (double)sum / employees.Count;
            }

            return 0.0;
        }
    }
}
