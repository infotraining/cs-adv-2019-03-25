﻿namespace Models
{
    public class PointInfo
    {
        public string Description { get; set; }

        public PointInfo(string description)
        {
            Description = description;
        }
    }
}
