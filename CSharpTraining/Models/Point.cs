﻿using System;

namespace Models
{
    public struct Point
    {
        private int X;
        private int Y;

        public PointInfo Info { get; set; }

        public Point(int x, int y, string info)
        {
            X = x;
            Y = y;
            Info = new PointInfo(info);
        }

        public void Move(int deltaX, int deltaY)
        {
            X += deltaX;
            Y += deltaY;
        }

        public void PrintPosition()
        {
            Console.WriteLine($"x, y = {X}, {Y}, opis: {Info.Description}");
        }
    }
}
