﻿using Newtonsoft.Json;
using System;

namespace Models
{
    [Serializable]
    public class Employee : System.Object, ICloneable
    {
        public enum PerformanceGrade
        {
            None,
            Awful,
            Bad,
            Decent,
            Good,
            Stellar
        }

        [JsonProperty]
        private int id;

        public string Name { get; set; }
        public PerformanceGrade Grade { get; set; }
        
        public Contract Contract { get; set; }

        public Employee()
        {
            Grade = PerformanceGrade.None;
            Contract = new Contract();
        }

        public Employee(int id, string name) : this()
        {
            this.id = id;
            Name = name;
        }

        public Employee(int id, string name, PerformanceGrade grade) : this(id, name)
        {
            Grade = grade;
        }


        public Employee(int id, string name, PerformanceGrade grade, decimal salary)
            : this(id, name, grade)
        {
            Contract.Salary = salary;
        }

        public Employee(int id, string name, PerformanceGrade grade, decimal salary,
            DateTime signed, DateTime expires) : this(id, name, grade, salary)
        {
            Contract.Salary = salary;
            Contract.DateSigned = signed;
            Contract.DateExpires = expires;
        }

        public int GetId()
        {
            return id;
        }

        public void GiveRaise(double percentage)
        {
            this.Contract.Salary = (decimal)((double)this.Contract.Salary * (1 + percentage / 100));
        }

        public object Clone()
        {
            Employee emp = (Employee)this.MemberwiseClone();

            emp.Contract = (Contract)emp.Contract.Clone();

            return emp;
        }

        public override string ToString()
        {
            //return string.Format("{0}, {1}", id, Name);
            return $"ID: {id}, Imię: {Name}, Pensja: {Contract.Salary}, " +
                $"Ocena: {Grade}, kontrakt do: {Contract.DateExpires.ToShortDateString()}";
        }

        public override int GetHashCode()
        {
            return Contract.GetContractId().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Employee && obj != null)
            {
                Employee temp = obj as Employee;

                if (this.id == temp.id && this.Name == temp.Name && this.Grade == temp.Grade
                    && this.Contract.GetContractId() == temp.Contract.GetContractId()
                    && this.Contract.Salary == temp.Contract.Salary 
                    && this.Contract.DateSigned == temp.Contract.DateSigned
                    && this.Contract.DateExpires == temp.Contract.DateExpires)
                {
                    return true;
                }

                return false;
            }

            return false;
        }
    }
}
