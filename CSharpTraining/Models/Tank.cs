﻿using System;
using System.Threading;

namespace Models
{
    public class Tank
    {
        private Engine Engine;
        private bool overheated;

        public const int CriticalSpeed = 150;

        public int CurrentSpeed { get; private set; }
        public string Name { get; set; }

        public Tank(string name)
        {
            Name = name;
            Engine = new Engine();
        }

        public void TurnKey(bool on)
        {
            Engine.PowerUp(on);
        }

        public void PerformReset()
        {
            Thread.Sleep(3000);
            overheated = false;
            Console.WriteLine("-- Silnik jest w pełni sprawny! --");
        }

        public void SpeedUp(int delta)
        {
            if (!Engine.IsRunning())
            {
                //Console.WriteLine("Nie włączono silnika!");
                throw new InvalidOperationException();
            }
            else if (overheated)
            {
                //Console.WriteLine($"{Name} się przegrzał");
                throw new ArgumentException();
            }
            else if (delta > 0)
            {
                CurrentSpeed += delta;

                if (CurrentSpeed > CriticalSpeed)
                {
                    Engine.PowerUp(false);
                    CurrentSpeed = 0;
                    overheated = true;

                    //Console.WriteLine($"{Name} się przegrzał i nie działa !!!");
                    //Exception ex = new Exception($"{Name} się przegrzał i nie działa !!!");

                    //ex.HelpLink = "http://www.infotraining.pl";

                    //ex.Data.Add("Czas wystąpienia", DateTime.Now);
                    //ex.Data.Add("Przyczyna", $"Przekroczona została prędkość krytyczna > {CriticalSpeed}");
                    
                    throw new TankOverheatedException($"{Name} się przegrzał i nie działa !!!",
                        DateTime.Now, $"Przekroczona została prędkość krytyczna > {CriticalSpeed}");
                }
                else
                {
                    Console.WriteLine($"> Prędkość = {CurrentSpeed}");
                }
            }
        }
    }

}
