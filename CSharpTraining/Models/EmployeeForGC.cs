﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Models
{
    public class EmployeeForGC : Employee, IDisposable
    {
        //zasób niezarządzany przez GC
        public readonly IntPtr EmployeeInfo;
        //zasób zarządzany przez GC
        public readonly SqlConnection Connection;

        public EmployeeForGC(int id, string name, PerformanceGrade grade) : base(id, name, grade)
        {
            EmployeeInfo = Marshal.StringToHGlobalAnsi($"{GetId()};{Name};{Grade}");
            Connection = new SqlConnection(
                @"Data Source=(localdb)\mssqllocaldb;Initial Catalog=master;Integrated Security=True");
        }

        private bool disposed;

        private void CleanUp(bool disposing)
        {
            if (disposed)
                return;

            //zwalniamy zasoby zarządzane
            if (disposing)
            {
                Connection.Dispose();
            }

            //zwalniamy niezarządzane zasoby
            Marshal.FreeHGlobal(EmployeeInfo);

            disposed = true;
        }

        ~EmployeeForGC()
        {
            CleanUp(false);
            Debug.WriteLine("Obiekt został zniszczony");
        }

        public void Dispose()
        {
            CleanUp(true);
            GC.SuppressFinalize(this);
            Debug.WriteLine("Zasoby zostały zwolnione");
        }
    }
}
