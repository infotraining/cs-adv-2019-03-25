﻿using System;

namespace Models
{
    [Serializable]
    public class TankOverheatedException : ApplicationException
    {
        public DateTime TimeStamp { get; set; }
        public string Cause { get; set; }

        public TankOverheatedException(string message, DateTime timeStamp, string cause) : base(message)
        {
            TimeStamp = timeStamp;
            Cause = cause;
        }

        public TankOverheatedException(string message, Exception inner, DateTime timeStamp, string cause)
            : base(message, inner)
        {
            TimeStamp = timeStamp;
            Cause = cause;
        }
    }
}
