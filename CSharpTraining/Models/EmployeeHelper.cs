﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public delegate bool IsPromotable(Employee employee);

    public static class EmployeeHelper
    {
        public static void CheckForPromotion(List<Employee> employees)
        {
            foreach (Employee employee in employees)
            {
                if (employee.Grade > Employee.PerformanceGrade.Decent)
                {
                    Console.WriteLine($"{employee.Name} może dostać podwyżkę!");
                }
            }
        }

        public static void BetterCheckForPromotions(List<Employee> employees, IsPromotable promotable)
        {
            foreach (Employee employee in employees)
            {
                if (promotable(employee))
                {
                    Console.WriteLine($"{employee.Name} może dostać podwyżkę!");
                }
            }
        }

        public static void BestCheckForPromotions(List<Employee> employees, Predicate<Employee> promotable)
        {
            foreach (Employee employee in employees)
            {
                if (promotable(employee))
                {
                    Console.WriteLine($"{employee.Name} może dostać podwyżkę!");
                }
            }
        }
    }
}
