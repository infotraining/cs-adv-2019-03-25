﻿using System;

namespace Models
{
    public class Engine
    {
        private bool isOn;

        public void PowerUp(bool on)
        {
            if (!isOn & on)
            {
                isOn = true;
                Console.WriteLine("Silnik włączony...");
            }
            else if (isOn & !on)
            {
                isOn = false;
                Console.WriteLine("Silnik wyłączony...");
            }
        }

        public bool IsRunning()
        {
            return isOn;
        }
    }
}
