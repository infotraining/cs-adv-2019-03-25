﻿using Newtonsoft.Json;
using System;

namespace Models
{
    [Serializable]
    public class Contract : ICloneable
    {
        [JsonProperty]
        private Guid contractId;

        public decimal Salary { get; set; }
        public DateTime DateSigned { get; set; }
        public DateTime DateExpires { get; set; }

        public Contract()
        {
            contractId = Guid.NewGuid();

            Salary = 2500M;
            DateSigned = DateTime.Now;
            DateExpires = DateTime.Now.AddYears(2);
        }

        public Contract(decimal salary) : this()
        {
            Salary = salary;
        }

        public Contract(decimal salary, DateTime signed, DateTime expires) : this(salary)
        {
            DateSigned = signed;
            DateExpires = expires;
        }

        public Guid GetContractId()
        {
            return contractId;
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
