﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqToEF
{
    class Program
    {
        static void Main(string[] args)
        {
            //InsertData();

            QueryData();

            Console.ReadLine();
        }

        static void QueryData()
        {
            using (StoreDatabaseContext context = new StoreDatabaseContext())
            {
                context.Database.Log = Console.WriteLine;

                var query = context.Products/*.AsEnumerable()*/.First(p => p.Price > 1000M);

                Console.WriteLine();
                Console.WriteLine(query.Description);
            }
        }

        static void InsertData()
        {
            using (StoreDatabaseContext context = new StoreDatabaseContext())
            {
                Store s1 = new Store { Name = "Amazon" };
                Store s2 = new Store { Name = "Ebay" };

                Product p1 = new Product
                {
                    Description = "IPhone",
                    Price = 2500M,
                    DateCreated = new DateTime(2018, 2, 3)
                };
                Product p2 = new Product
                {
                    Description = "Huawei",
                    Price = 500M,
                    DateCreated = new DateTime(2018, 10, 4)
                };
                Product p3 = new Product
                {
                    Description = "HTC",
                    Price = 1500M,
                    DateCreated = new DateTime(2017, 6, 8)
                };
                Product p4 = new Product
                {
                    Description = "Sony",
                    Price = 1400M,
                    DateCreated = new DateTime(2019, 1, 31)
                };
                Product p5 = new Product
                {
                    Description = "Samsung",
                    Price = 1800M,
                    DateCreated = new DateTime(2018, 12, 22)
                };
                Product p6 = new Product
                {
                    Description = "HTC",
                    Price = 1400M,
                    DateCreated = new DateTime(2018, 10, 4)
                };
                Product p7 = new Product
                {
                    Description = "Huwaei",
                    Price = 500M,
                    DateCreated = new DateTime(2018, 10, 4)
                };

                context.Stores.Add(s1);
                context.Stores.Add(s2);

                s1.Products.AddRange(new List<Product> { p1, p3, p5, p7 });
                s2.Products.AddRange(new List<Product> { p2, p4, p6 });

                context.SaveChanges();
            }
        }
    }
}
