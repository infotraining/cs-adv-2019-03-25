﻿using System.Data.Entity;

namespace LinqToEF
{
    public class StoreDatabaseContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Store> Stores { get; set; }
    }
}
