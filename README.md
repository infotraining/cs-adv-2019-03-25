# Programowanie w jezyku CSharp

## Ankieta

* https://infotraining.pl/ankieta/cs-basic-2019-03-25-lz

# Wzorce projektowe w jezyku CSharp

## Dokumentacja

* https://infotraining.bitbucket.io/cs-dp/

## Prezentacja

* https://gitpitch.com/infotraining-dev/cs-dp-slides/master?grs=bitbucket#/

## Ankieta - Design Patterns

* https://www.infotraining.pl/ankieta/cs-dp-2019-03-27-kp