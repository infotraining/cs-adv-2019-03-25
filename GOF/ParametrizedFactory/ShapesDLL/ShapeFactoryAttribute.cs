﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Drawing
{
    public sealed class ShapeFactoryAttribute : Attribute
    {
        public string ShapeId { get; set; }
    }
}
