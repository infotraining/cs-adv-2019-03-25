﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;

namespace Drawing
{
    public struct Point
    {
        public int X;
        public int Y;
        
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return string.Format("[{0}, {1}]", X, Y);
        }
    }

    public interface IShape
    {
        void Draw();
        void Move(int dx, int dy);
    }

    public abstract class Shape : IShape
    {
        public Point Coordinates { get; set; }

        #region IShape Members

        public abstract void Draw();

        public virtual void Move(int dx, int dy)
        {
            Coordinates = new Point(Coordinates.X + dx, Coordinates.Y + dy);            
        }

        #endregion    
    }

    public abstract class ShapeFactory
    {
        static ShapeFactory()
        {
            Assembly asmbl = Assembly.GetCallingAssembly();

            Type factoryAttr = new ShapeFactoryAttribute().GetType();

            var factories = from f in asmbl.GetTypes()
                            where f.GetCustomAttributes(factoryAttr, false).Length > 0
                            select f;

            foreach (var f in factories)
            {
                ShapeFactory factory = (ShapeFactory)Activator.CreateInstance(f);

                ShapeFactoryAttribute attr = (ShapeFactoryAttribute)f.GetCustomAttributes(factoryAttr, false).First();
                Register(attr.ShapeId, factory);
            }
        }

        private static IDictionary<string, ShapeFactory> creators = new Dictionary<string, ShapeFactory>();

        protected abstract IShape CreateShape(params object[] args);

        public static IShape Create(string id, params object[] args)
        {
            return creators[id].CreateShape(args);
        }

        public static void Register(string id, ShapeFactory creator)
        {
            creators.Add(id, creator);
        }

        public static void Unregister(string id)
        {
            creators.Remove(id);
        }
    }
}
