﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    public class Rectangle : Shape
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public Rectangle() : this(0, 0, 0, 0)
        {
        }

        public Rectangle(int x, int y, int width, int height)
        {
            Coordinates = new Point(x, y);
            Width = width;
            Height = height;
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a rectangle at {0} with w: {1} & h: {2}", Coordinates, Width, Height);            
        }

        [ShapeFactory(ShapeId = "Rectangle")]
        public class RectangleFactory : ShapeFactory
        {
            protected override IShape CreateShape(params object[] args)
            {
                XElement rectElement = (XElement)args[0];
                int x = int.Parse(rectElement.Element("Point").Element("X").Value);
                int y = int.Parse(rectElement.Element("Point").Element("Y").Value);
                int width = int.Parse(rectElement.Element("Width").Value);
                int height = int.Parse(rectElement.Element("Height").Value);

                return new Rectangle(x, y, width, height);
            }
        }

    }
}
