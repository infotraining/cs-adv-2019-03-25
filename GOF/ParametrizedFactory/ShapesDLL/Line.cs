﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Drawing
{
    public class Line : Shape
    {
        public Point EndOfLine { get; set; }

        public Line(int x1, int y1, int x2, int y2)
        {
            Coordinates = new Point(x1, y1);
            EndOfLine = new Point(x2, y2);
        }

        public override void Draw()
        {
            Console.WriteLine("Drawing a line from {0} to {1}", Coordinates, EndOfLine);
        }

        [ShapeFactory(ShapeId = "Line")]
        public class LineFactory : ShapeFactory
        {

            protected override IShape CreateShape(params object[] args)
            {
                XElement lineElement = (XElement)args[0];

                var points = lineElement.Elements("Point");

                int x1 = int.Parse(points.ElementAt(0).Element("X").Value);
                int y1 = int.Parse(points.ElementAt(0).Element("Y").Value);
                int x2 = int.Parse(points.ElementAt(1).Element("X").Value);
                int y2 = int.Parse(points.ElementAt(1).Element("Y").Value);

                return new Line(x1, y1, x2, y2);
            }
        }
    }
}