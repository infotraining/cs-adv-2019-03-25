﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportBuilderApp
{
    public interface IReportBuilder
    {
        IReportBuilder AddHeader(string header);
        IReportBuilder BeginData();
        IReportBuilder EndData();
        IReportBuilder AddRow(params string[] dataRow);
        IReportBuilder AddFooter(string footer);
    }

    public class HtmlReportBuilder : IReportBuilder
    {
        StringBuilder str = new StringBuilder();

        public IReportBuilder AddFooter(string footer)
        {
            str.AppendFormat("<div class='footer'>{0}</div>", footer);

            return this;
        }

        public IReportBuilder AddHeader(string header)
        {
            str.Clear();
            str.AppendFormat("<h1>{0}</h1>\n", header);

            return this;
        }

        public IReportBuilder AddRow(params string[] dataRow)
        {
            str.Append("  <tr>\n");
            
            foreach(var item in dataRow)
            {
                str.AppendFormat("    <td>{0}</td>\n", item);
            }
            str.Append("  </tr>\n");

            return this;
        }

        public IReportBuilder BeginData()
        {
            str.Append("<table>\n");

            return this;
        }

        public IReportBuilder EndData()
        {
            str.Append("</table>\n");

            return this;
        }

        public string GetHtml()
        {
            return str.ToString();
        }
    }

    public class CsvReportBuilder : IReportBuilder
    {
        List<string> lines = new List<string>();

        public IReportBuilder AddFooter(string footer)
        {
            lines.Add(string.Format("# {0}\n", footer));
            return this;
        }

        public IReportBuilder AddHeader(string header)
        {
            lines.Clear();
            lines.Add(string.Format("# {0}\n",header));
            return this;
        }

        public IReportBuilder AddRow(params string[] dataRow)
        {
            StringBuilder str = new StringBuilder();
            foreach (var item in dataRow)
            {
                str.AppendFormat("{0}; ", item);
            }            
            lines.Add(str.ToString());

            return this;
        }

        public IReportBuilder BeginData()
        {
            return this;
        }

        public IReportBuilder EndData()
        {
            return this;
        }

        public List<string> GetCsv()
        {
            return lines;
        }
    }

    public class DataParser
    {
        public DataParser(IReportBuilder reportBuilder)
        {
            ReportBuilder = reportBuilder;
        }

        public IReportBuilder ReportBuilder { get; set; }

        public void ParseDataFile(string fileName)
        {
            using (TextReader reader = File.OpenText(fileName))
            {
                ReportBuilder.AddHeader("Report from: " + fileName);

                ReportBuilder.BeginData();
                
                while(true)
                {
                    string line = reader.ReadLine();

                    if (line == null)
                        break;

                    var data = line.Split();
                    ReportBuilder.AddRow(data);
                }

                ReportBuilder.EndData();

                ReportBuilder.AddFooter("Copyrights");
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            HtmlReportBuilder htmlReportBuilder = new HtmlReportBuilder();

            htmlReportBuilder
                .AddHeader("title")
                .BeginData()
                .AddRow("1", "one")
                .AddRow("2", "two")
                .EndData()
                .AddFooter("copyright");

            Console.WriteLine(htmlReportBuilder.GetHtml());

            Console.WriteLine("\n----------------\n");

            DataParser parser = new DataParser(htmlReportBuilder);
            parser.ParseDataFile("data.txt");
            var htmlReport = htmlReportBuilder.GetHtml();

            Console.WriteLine(htmlReport);

            Console.WriteLine("\n----------------\n");

            var csvReportBuilder = new CsvReportBuilder();
            parser = new DataParser(csvReportBuilder);
            parser.ParseDataFile("data.txt");
            var csvReport = csvReportBuilder.GetCsv();

            foreach (var line in csvReport)
            {
                Console.WriteLine(line);
            }
        }
    }
}
