﻿namespace DP.Creational.AbstractFactory.Example.NET
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblSql = new System.Windows.Forms.Label();
      this.txtSql = new System.Windows.Forms.TextBox();
      this.lblResults = new System.Windows.Forms.Label();
      this.dgvResults = new System.Windows.Forms.DataGridView();
      this.btnExecute = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).BeginInit();
      this.SuspendLayout();
      // 
      // lblSql
      // 
      this.lblSql.AutoSize = true;
      this.lblSql.Location = new System.Drawing.Point(13, 13);
      this.lblSql.Name = "lblSql";
      this.lblSql.Size = new System.Drawing.Size(31, 13);
      this.lblSql.TabIndex = 0;
      this.lblSql.Text = "SQL:";
      // 
      // txtSql
      // 
      this.txtSql.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.txtSql.Location = new System.Drawing.Point(50, 10);
      this.txtSql.Name = "txtSql";
      this.txtSql.Size = new System.Drawing.Size(398, 20);
      this.txtSql.TabIndex = 1;
      // 
      // lblResults
      // 
      this.lblResults.AutoSize = true;
      this.lblResults.Location = new System.Drawing.Point(13, 47);
      this.lblResults.Name = "lblResults";
      this.lblResults.Size = new System.Drawing.Size(45, 13);
      this.lblResults.TabIndex = 2;
      this.lblResults.Text = "Results:";
      // 
      // dgvResults
      // 
      this.dgvResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.dgvResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvResults.Location = new System.Drawing.Point(16, 64);
      this.dgvResults.Name = "dgvResults";
      this.dgvResults.Size = new System.Drawing.Size(512, 257);
      this.dgvResults.TabIndex = 3;
      // 
      // btnExecute
      // 
      this.btnExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.btnExecute.Location = new System.Drawing.Point(454, 8);
      this.btnExecute.Name = "btnExecute";
      this.btnExecute.Size = new System.Drawing.Size(74, 23);
      this.btnExecute.TabIndex = 4;
      this.btnExecute.Text = "Execute";
      this.btnExecute.UseVisualStyleBackColor = true;
      this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(540, 333);
      this.Controls.Add(this.btnExecute);
      this.Controls.Add(this.dgvResults);
      this.Controls.Add(this.lblResults);
      this.Controls.Add(this.txtSql);
      this.Controls.Add(this.lblSql);
      this.Name = "MainForm";
      this.Text = "NORTHWIND";
      ((System.ComponentModel.ISupportInitialize)(this.dgvResults)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblSql;
    private System.Windows.Forms.TextBox txtSql;
    private System.Windows.Forms.Label lblResults;
    private System.Windows.Forms.DataGridView dgvResults;
    private System.Windows.Forms.Button btnExecute;
  }
}

