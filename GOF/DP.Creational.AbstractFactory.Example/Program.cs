﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DP.Creational.AbstractFactory.Example
{
    public enum GameLevel
    {
        Easy,
        Hard,
        DieHard
    };

    public class Game
    {
        MonsterFactory _monsterFactory;
        List<Enemy> _enemies = new List<Enemy>();
            
        public Game(GameLevel gameLevel)
        {
            switch (gameLevel)
            {
                case GameLevel.Easy:
                    _monsterFactory = new SillyMonsterFactory();
                    break;
                case GameLevel.Hard:
                    _monsterFactory = new BadMonsterFactory();
                    break;
            }
        }

        public void Initialize()
        {
            Random random = new Random();

            for (int i = 0; i < 10; ++i)
            {
                int rand = random.Next(10);
                if (rand < 3)
                    _enemies.Add(_monsterFactory.CreateSoldier());
                else if ((rand >= 3) && (rand <= 6))
                    _enemies.Add(_monsterFactory.CreateMonster());
                else
                    _enemies.Add(_monsterFactory.CreateSuperMonster());
            }
        }

        public void Run()
        {
            // fighting
            foreach (Enemy e in _enemies)
                e.Fight();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game(GameLevel.Hard);
            game.Initialize();
            game.Run();
            

            Console.ReadKey();
        }
    }
}
