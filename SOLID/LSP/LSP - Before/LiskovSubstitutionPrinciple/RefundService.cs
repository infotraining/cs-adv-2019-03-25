﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovSubstitutionPrinciple
{
    public class RefundService
    {
        public RefundResponse Refund(RefundRequest refundRequest)
        {
            PaymentServiceBase paymentService = PaymentServiceFactory.GetPaymentServiceFrom(refundRequest.Payment);

            RefundResponse refundResponse = new RefundResponse();

            if ((paymentService as PayPalPayment) != null)
            {
                ((PayPalPayment) paymentService).AccountName = "Scott123-PP";
                ((PayPalPayment)paymentService).Password = "245sdsdfg";
            }

            if ((paymentService as WorldPayPayment) != null)
            {
                ((WorldPayPayment) paymentService).AccountId = "Scott123-WP";
                ((WorldPayPayment)paymentService).AccountPassword = "ASDASDP";
                ((WorldPayPayment)paymentService).ProductId = "1";
            }

            string merchantResponse = paymentService.Refund(refundRequest.RefundAmount,
                                                            refundRequest.PaymentTransactionId);

            refundResponse.Message = merchantResponse;

            if (merchantResponse.Contains("A_Success") || merchantResponse.Contains("Auth"))
                refundResponse.Success = true;
            else
                refundResponse.Success = false;

            return refundResponse;
        }
    }
}
