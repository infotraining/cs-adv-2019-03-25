﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LedDevices;
using System.IO;

namespace LightSwitching
{
    public interface ILogger
    {
        void Log(string message);
    }

    public class FileLogger : ILogger
    {
        string _fileName;

        public FileLogger(string fileName)
        {
            _fileName = fileName;
        }

        public void Log(string message)
        {
            using (var logFile = new StreamWriter(_fileName, true))
            {
                logFile.WriteLine(message);
            }
        }
    }

    public interface ISwitch
    {
        void On();

        void Off();

        bool IsOn { get; }
    }

    public class LedSwitch : ISwitch
    {
        LedLight _ledLight;

        public LedSwitch(LedLight ledLight)
        {
            _ledLight = ledLight;
        }

        public bool IsOn { get ; protected set; }

        public void Off()
        {
            _ledLight.SetRGB(0, 0, 0);
            IsOn = false;
        }

        public void On()
        {
            _ledLight.SetRGB(255, 255, 255);
            IsOn = true;
        }
    }


    public class Button
    {
        ISwitch _switch;
        ILogger _logger;        

        public Button(ISwitch s, ILogger l)
        {
            _switch = s;
            _logger = l;
        }

        public void Click()
        {
            if (_switch.IsOn)
            {
                _switch.Off();
            }
            else
            {
                _switch.On();
            }

            _logger.Log("Button was clicked... " + (_switch.IsOn ? "ON" : "OFF"));
        }
    }

    class Program
    {       
        static void Main(string[] args)
        {
            SimpleInjector.Container container = new SimpleInjector.Container();
            container.Register<ISwitch, LedSwitch>();
            container.Register<ILogger>(() => new FileLogger("log.dat"));

            Button btn = container.GetInstance<Button>();

            btn.Click();
            btn.Click();
            btn.Click();
            btn.Click();
            btn.Click();
        }
    }
}
